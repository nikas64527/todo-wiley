'use strict';
import React from 'react';
import ReactDOM from 'react-dom';

import ToDoEditor from './components/ToDoEditor';
import ToDoList from './components/ToDoList';


const DEFAULT_TODO_LIST = [
	{
		id : 1,
		text: 'Написить ToDo лист',
		checked: false,
	},
	{
		id : 2,
		text: 'Отправить на проверку',
		checked: false
	},
	{
		id : 3,
		text: 'Пройти собеседование',
		checked: false
	},
	{
		id : 4,
		text: 'Прийти к соглашению на собеседовании',
		checked: false
	},
	{
		id : 4,
		text: 'Приступить к работе',
		checked: false
	},
];
const NAMESPACE = 'ToDoList';
class App extends React.Component {
	constructor(props) {
		super(props);
		this.checkDataInLocalStorage();
		// this.SaveDefaultNotesToLocalStorage();
		/*if (! localStorage.getItem(NAMESPACE)) {
			store(DEFAULT_TODO_LIST);
		};*/
		// console.log(this.getToDoListFromLocalStorage());
		
	}
	/*componentDidMount() {
		this.checkDataInLocalStorage();
	}*/
	checkDataInLocalStorage() {
		let ToDoList = this.getToDoListFromLocalStorage();
		if (ToDoList.length) {
			this.state = {
				notes: ToDoList
			};
		} else {
			this.changeLocalStorage('add', DEFAULT_TODO_LIST);
			// this.saveToLocalStorage(DEFAULT_TODO_LIST);
			this.state = {
				notes: DEFAULT_TODO_LIST
			};
		}
	}
	/*SaveDefaultNotesToLocalStorage() {
		if (! this.getToDoListFromLocalStorage().length) {
			this.saveToLocalStorage(DEFAULT_TODO_LIST);
		}
	}*/
	/*saveToLocalStorage(newNote) {
		let storage = this.getToDoListFromLocalStorage();
		if (Array.isArray(newNote)) {
			storage.push(...newNote);
		} else {
			storage.push(newNote);
		}
		localStorage.setItem(NAMESPACE, JSON.stringify(storage));
	}*/
	/*deleteFromLocalStorage(id) {
		let storage = this.getToDoListFromLocalStorage();
		let filteredStorage = storage.filter(note => note.id != id);
		localStorage.setItem(NAMESPACE, JSON.stringify(filteredStorage));
	}*/
	getToDoListFromLocalStorage() {
		let storage = localStorage.getItem(NAMESPACE);
		return (storage && JSON.parse(storage)) || [];
	}
	changeLocalStorage(action, data) {
		let storage = this.getToDoListFromLocalStorage();
		switch (action) {
			case 'add':
				if (Array.isArray(data)) {
					storage.push(...data);
				} else {
					storage.push(data);
				}
				localStorage.setItem(NAMESPACE, JSON.stringify(storage));
			break;

			case 'delete':
				let filteredStorage = storage.filter(note => note.id != data);
				localStorage.setItem(NAMESPACE, JSON.stringify(filteredStorage));
			break;

			case 'replace':
				localStorage.setItem(NAMESPACE, JSON.stringify(data));
			break;

			case 'saveToLocalStorage':
			break;

			case 'saveToLocalStorage':
			break;
		}
	}
	/*handleNoteAdd(newNote) {
		// console.log(newNote);
		this.setState({
			notes: [ ... this.state.notes, newNote]
		})
	}*/
	ToDoItemAdd(newNote) {
		this.setState({
			notes: [ ... this.state.notes, newNote]
		})
		// this.saveToLocalStorage(newNote);
		this.changeLocalStorage('add', newNote);
		// store(newNote);
	}
	toDoItemDelete(id) {
		// console.log(id);
		this.setState({
			notes: this.state.notes.filter(note => note.id != id)
		})
		// this.deleteFromLocalStorage(id);
		this.changeLocalStorage('delete', id)
	}
	/*toDoItemCheckedChange(id, checked) {
		console.log(id+' '+checked)
	}*/
	toDoItemChange(id, prop, value) {
		let notes = this.state.notes.map(note => {
			note.id === id ? {...note, [prop] : value} : note;
		});
		/*
		const notes = this.state.notes.map(note => {
			if (note.id == id) {
				// console.log(note);
				note[prop] = value;
			}
			return note;
		});
		*/
		this.setState({
			notes: notes
		})
		this.changeLocalStorage('replace', notes);
	}
	render() {
		return (
	    	<div>
	    		<h1>ToDo list</h1>
	    		<button>Сортировать по времени </button>
	    		<button>Сортировать по названию</button>
	    		<button>Скрыть выполненные</button>
	    		<ToDoList 
	    			notes={this.state.notes} 
	    			toDoItemDelete={this.toDoItemDelete.bind(this)}
	    			toDoItemChange={this.toDoItemChange.bind(this)}
	    		/>
	    		<ToDoEditor 
	    			onNoteAdd={this.ToDoItemAdd.bind(this)}
	    		/>
	    	</div>
		);
	}
}
/*function store(data) {
	if (data) {
		return localStorage.setItem(NAMESPACE, JSON.stringify(data));
	}
	var store = localStorage.getItem(NAMESPACE);
	return (store && JSON.parse(store)) || [];
}*/

ReactDOM.render(<App/>, document.getElementById('app'));