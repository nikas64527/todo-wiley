import React from 'react';
export default class ToDoEditor extends React.Component {
	constructor(props) {
		super(props);
		this.state = {};
		// this.onChange = this.onChange.bind(this);
	}
	handleTextChange(e) {
		this.setState({
			text: e.target.value
		});
	}
	handleToDoItemAdd() {
		const newNote = {
			id: Date.now(),
			text: this.state.text,
			checked: false,
		};
		this.clearStateText();
		this.props.onNoteAdd(newNote);
	}
	handleToDoItemDelete() {

	}
	clearStateText() {
		this.setState({
			text: ''
		});
	}
	render() {
		return (
	    	<div>
	    		<textarea
	    			placeholder="Введите текст задачи"
	    			value={this.state.text}
	    			onChange={this.handleTextChange.bind(this)}/>
	    		<button onClick={this.handleToDoItemAdd.bind(this)} disabled={!this.state.text}>Добавить</button>
	    	</div>
		);
	}
}